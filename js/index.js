/**
 * Created by Lalit Umbarkar
 * Date: 2/9/17
 * Project: js-plant
 */



function validate(id) {

    var code = document.getElementById(id).textContent;
    notify(true, code);
    code = formatter.formatJson(code.trim(), "\t");
    document.getElementById(id).textContent = code;

    try {
        jsonlint.parse(code);
        notify(true, 'Valid JSON');
        document.getElementById(id).textContent = code;
    } catch (e) {
        notify(false, "error");
        // retrieve line number from error string
        var lineMatches = e.message.match(/line ([0-9]*)/);

        if (lineMatches && lineMatches.length > 1) {
            // this.highlightErrorLine(+lineMatches[1] - 1);
        }
        notify(false, lineMatches + " - " +  e.message);
    }
}

function notify(status, message) {
    if (status)
        console.log(message);
    else
        console.error(message);
}




